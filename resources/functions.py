import string 
import random

def get_password_len():
    l = int(input('Type the lenght of the new password: '))
    return(l)

def create_random_password(l):
    pool = string.ascii_uppercase + string.ascii_lowercase + string.digits
    password = ''.join(random.sample(pool,l))
    return(password)

def total_options(l):
    return(63**l)